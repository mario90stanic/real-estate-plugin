<?php 
require_once('../../../../wp-load.php');

/** function for updateting afc fields */
function text_field_update( $real_estate_id, $field, $value, $filter ) {
    if( isset( $value ) && $value != "" ) {
        $subtitle        = filter_var( $value, $filter );
        $result          = update_post_meta( $real_estate_id, $field, $subtitle );

        return array(
            'status' => $result, 
            'value'  => $value
        );
    }
}

/** Function for updateing taxonomies */
function term_update( $real_estate_id, $value, $term, $filter ) {
    if( isset( $value ) && $value != "" ) {
        $subtitle        = filter_var($value, $filter);
        $result          = wp_set_post_terms( $real_estate_id, $value, $term, false );

        $term = get_term($result[0]);
        $term_link = get_term_link($term);

        return array(
            'status'   => $result, 
            'term'     => $term->name,
            'taxonomy' => $term->taxonomy,
            'link'      => $term_link,
        );
    }
}

/** Checking if the real estate ID is sent and doing update */
if( isset( $_POST['real_estate_id'] ) && $_POST['real_estate_id'] > 0 ) {
    $respons             = array();
    $real_estate_id      = filter_var( $_POST['real_estate_id'], FILTER_VALIDATE_INT );
    
    if( isset( $_POST['title'] ) && $_POST['title'] != '' ) {
        $args = array(
            'ID'         => $real_estate_id,
            'post_title' => filter_var( $_POST['title'], FILTER_SANITIZE_STRING ),
        );
        
        $result          = array(
            'status' => wp_update_post( $args ), 
            'value'  => $_POST['title']
        );

        $respons['title'] =  $result;
    }
    
    $respons['subtitle'] = text_field_update( $real_estate_id, 'subtitle', $_POST['subtitle'], FILTER_SANITIZE_STRING );
    $respons['location'] = term_update( $real_estate_id, $_POST['location'], 'location', FILTER_SANITIZE_STRING );
    $respons['type']     = term_update( $real_estate_id, $_POST['type'], 'type', FILTER_SANITIZE_STRING );

    echo json_encode( $respons );    
}