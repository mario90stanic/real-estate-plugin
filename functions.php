<?php
/** 
 * Requiring all of the functionality 
 */

require_once("functions/taxonomy-validation.php");
require_once("functions/rewrite-url.php");
require_once("functions/search.php");
require_once("functions/enque-scripts.php");
require_once("functions/register-templates.php");