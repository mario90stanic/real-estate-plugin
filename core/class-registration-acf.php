<?php
/**
 * Class for creating required ACF fields
 */

class registration_acf extends base{

    private $taxonomy_name;

    public function __construct( $name ) {
        $this->taxonomy_name = $this->make_slug( $name );
        $this->create_acf_fields();
    }

    /** Creating ACF for Real Estate CPT */
    private function create_acf_fields() {
        if( function_exists( "acf_add_local_field_group" ) ):
            acf_add_local_field_group( array(
                "key"    => "group_5d2500be77bd2",
                "title"  => "Real Estate additionl fields",
                "fields" => array(
                    array(
                        "key"               => "field_5d2500e7c52f8",
                        "label"             => "Subtitle",
                        "name"              => "subtitle",
                        "type"              => "text",
                        "instructions"      => "",
                        "required"          => 0,
                        "conditional_logic" => 0,
                        "wrapper"           => array(
                            "width" => "",
                            "class" => "",
                            "id"    => "",
                        ),
                        "default_value"     => "",
                        "placeholder"       => "",
                        "prepend"           => "",
                        "append"            => "",
                        "maxlength"         => "",
                    ),
                    array(
                        "key"               => "field_5d2500fac52f9",
                        "label"             => "Gallery",
                        "name"              => "gallery",
                        "type"              => "gallery",
                        "instructions"      => "",
                        "required"          => 0,
                        "conditional_logic" => 0,
                        "wrapper"           => array(
                            "width" => "",
                            "class" => "",
                            "id"    => "",
                        ),
                        "min"               => "",
                        "max"               => "",
                        "insert"            => "append",
                        "library"           => "all",
                        "min_width"         => "",
                        "min_height"        => "",
                        "min_size"          => "",
                        "max_width"         => "",
                        "max_height"        => "",
                        "max_size"          => "",
                        "mime_types"        => "",
                    ),
                ),
                "location" => array(
                    array(
                        array(
                            "param"     => "post_type",
                            "operator"  => "==",
                            "value"     => $this->taxonomy_name,
                        ),
                    ),
                ),
                "menu_order"            => 0,
                "position"              => "normal",
                "style"                 => "default",
                "label_placement"       => "top",
                "instruction_placement" => "label",
                "hide_on_screen"        => "",
                "active"                => 1,
                "description"           => "",
            ));
        endif;
    }
}