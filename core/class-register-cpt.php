<?php 

/**
 * Registration of custom post type
 */

 class registration_cpt extends base {

    public $post_type_name;
    public $post_type_args;
    public $post_type_labels;

    public function __construct( $name ) {
        $this->post_type_name = $this->make_slug( $name );
        
        if( ! post_type_exists( $this->post_type_name ) ) {
            add_action( "init", array( $this, "register_post_type" ) );
        }
    }
    
    /** Registration of the cpt */
    public function register_post_type() {
        $name       = $this->make_name( $this->post_type_name );
        $plural     = $name . "s";
        $labels     = array(
                    "name"                  => _x( $plural, "post type general name" ),
                    "singular_name"         => _x( $name, "post type singular name" ),
                    "add_new"               => _x( "Add New", strtolower( $name ) ),
                    "add_new_item"          => __( "Add New " . $name ),
                    "edit_item"             => __( "Edit " . $name ),
                    "new_item"              => __( "New " . $name ),
                    "all_items"             => __( "All " . $plural ),
                    "view_item"             => __( "View " . $name ),
                    "search_items"          => __( "Search " . $plural ),
                    "not_found"             => __( "No " . strtolower( $plural ) . " found" ),
                    "not_found_in_trash"    => __( "No " . strtolower( $plural ) . " found in Trash" ),
                    "parent_item_colon"     => "",
                    "menu_name"             => $plural,        
        );
        
        $args       = array(
                    "label"                 => $plural,
                    "labels"                => $labels,
                    "public"                => true,
                    "show_ui"               => true,
                    "supports"              => array( "title", "editor" ),
                    "show_in_nav_menus"     => true,
                    "_builtin"              => false,
        );
        
        register_post_type( $this->post_type_name, $args );
    }

    /** Registration of the taxonomy */
    public function add_taxonomy( $name, $rewrite_slug ) {
        if( ! empty( $name ) ){
            $post_type_name = $this->post_type_name;
            $taxonomy_name  = $this->make_slug( $name );
            $name           = $this->make_name( $name );
            $plural         = $name . "s";
            $labels         = array(
                            "name"                  => _x( $plural, "taxonomy general name" ),
                            "singular_name"         => _x( $name, "taxonomy singular name" ),
                            "search_items"          => __( "Search " . $plural ),
                            "all_items"             => __( "All " . $plural ),
                            "parent_item"           => __( "Parent " . $name ),
                            "parent_item_colon"     => __( "Parent " . $name . ":" ),
                            "edit_item"             => __( "Edit " . $name ),
                            "update_item"           => __( "Update " . $name ),
                            "add_new_item"          => __( "Add New " . $name ),
                            "new_item_name"         => __( "New " . $name . " Name" ),
                            "menu_name"             => __( $name ),
            );
            
            $args           = array(
                            "label"                 => $plural,
                            "labels"                => $labels,
                            "public"                => true,
                            "show_ui"               => true,
                            "show_in_nav_menus"     => true,
                            "_builtin"              => false,
                            "hierarchical"          => false,
                            "rewrite"               => array(
                                "slug"       => $rewrite_slug,
                                "with_front" => true
                              )
            );
            
            add_action( "init", function() use( $taxonomy_name, $post_type_name, $args ) {
                register_taxonomy( $taxonomy_name, $post_type_name, $args );
            });
        }
    }
 }