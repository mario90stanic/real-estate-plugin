<?php

/**
 * Base class for common methods
 */

class base {
    /** creating the slug */
    public function make_slug( $name ) {
        $slug = strtolower( str_replace(" ", "_", $name ) );
        
        return $slug;
    }

    /** creating the name that has first upper letter in the words  */
    public function make_name( $name ) {
        $name = ucwords( str_replace( "_", " ", $name ) );
        
        return $name;
    }
}