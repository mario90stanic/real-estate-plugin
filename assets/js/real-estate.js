jQuery(function($){
    /** Replaceing term link on sinlge CPT Real Estate */
    function term_replace( result ) {
        if( result != undefined && result.status > 0 ) {
            $( "." + result.taxonomy + "-wrapp a" ).html( result.term );
            $( "." + result.taxonomy + "-wrapp a" ).attr('href', result.link);
        }
    }
    /** updating the values and replacing them on the page */ 
    $( ".update-btn" ).on( "click", function() {
        $.ajax({
            url: real_estate.plugins_url + "/real-estate-plugin/ajax/update-real-estate.php",
            type: "POST",
            data: {
                title:          $( ".title" ).val(),
                subtitle:       $( ".subtitle" ).val(),
                real_estate_id: $( ".real-estate-id" ).val(),
                location:       $( ".location" ).val(),
                type:           $( ".type" ).val(),
            },
            success: function(resource) {
                var result = JSON.parse(resource);

                if( result.title != undefined && result.title.status > 0 ) {
                    $( ".page-title" ).html( result.title.value );
                    $( ".title" ).val("");
                }

                if( result.subtitle != undefined && result.subtitle.status == true ) {
                    $( ".subtitle-wrapp" ).html( result.subtitle.value );
                    $( ".subtitle" ).val("");
                }

                term_replace( result.location );
                term_replace( result.type );               
            }
        }); 
    });
});