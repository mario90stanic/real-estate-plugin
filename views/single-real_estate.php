<?php
/**
 * The template for displaying Real estate cpt
 *
 * @package wp-test
 */

global $post;

$location = get_the_terms( get_the_ID(), "location" ); 
$type     = get_the_terms( get_the_ID(), "type" );

/** Cheking if there any taxonomies assing to this post */
if( isset( $location[0] ) ) {
    $location_name = $location[0]->name;
    $location_url  = get_term_link( $location[0] ); 
}

if( isset( $type[0]) ) {
    $type_name = $type[0]->name;
    $type_url  = get_term_link( $type[0] ); 
}

/** Getting all the terms for dropdowns */
function getTerms( $name ) {
    $terms = get_terms( array(
        "taxonomy"   => $name,
        "hide_empty" => false,
    ) );

    return $terms;
}

get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
            <h1>REAL ESTATE</h1>

            <?php while ( have_posts() ) : the_post();  ?>
                <h2 class="page-title"><?php the_title(); ?></h2>
                <p class="subtitle-wrapp"> <?php the_field("subtitle"); ?></p>
            <?php endwhile; ?>

            <?php $preview_gallery_images = get_field("gallery"); ?>
            <?php if ( $preview_gallery_images ) : ?>
                <?php foreach ($preview_gallery_images as $preview_gallery_image): ?>
                    <a href="<?php echo $preview_gallery_image["url"]; ?>">
                        <img src="<?php echo $preview_gallery_image["sizes"]["thumbnail"]; ?>" alt="<?php echo $preview_gallery_image["alt"]; ?>" />
                    </a>
                <?php endforeach; ?>
            <?php endif; ?>
            <div>
                <h3>Location</h3>
                <div class="location-wrapp">
                   <a href="<?php echo $location_url ?>"><?php echo $location_name ?></a>
                </div>
                <h3>Type</h3>
                <div class="type-wrapp">
                    <a href="<?php echo $type_url ?>"><?php echo $type_name ?></a>
                </div>
            </div>
            <?php if (current_user_can("author") && get_current_user_id() == $post->post_author || current_user_can("administrator")) { ?>
                <input class="title" type="text" name="title" placeholder="Title">
                <input class="subtitle" type="text" name="subtitle" placeholder="Subitle">

                <select class="location" name="location">
                    <?php foreach(getTerms("location") as $term) { ?>
                        <option value="<?php echo $term->slug ?>" <?php echo ($location_name == $term->slug) ? "selected" : null ?> ><?php echo $term->name ?></option>
                    <?php } ?>
                </select>

                <select class="type" name="type">
                    <?php foreach(getTerms("type") as $term) { ?>
                        <option value="<?php echo $term->slug ?>" <?php echo ($type_name == $term->slug) ? "selected" : null ?> ><?php echo $term->name ?></option>
                    <?php } ?>
                </select>

                <input class="real-estate-id" type="hidden" name="real-estate-id" value="<?php echo get_the_ID()?>" >
                <button class="update-btn">Change values</button>
            <?php } ?>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();