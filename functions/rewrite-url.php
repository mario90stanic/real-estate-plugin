<?php
/**
 * Rewriting the urls of the CPT Real estate
 */


/** Replaceing urls of the urls of the cpt Real Estate */
add_filter( "post_type_link", "rewrite", 1, 2 );
function rewrite( $link, $post = 0 ) {
    if( is_object( $post ) ) {
        $type = get_the_terms( $post->ID, "type" );

        if(is_array( $type )) {
            $type_slug = $type[0]->slug;
            
            if ( $post->post_type == "real_estate" ) { 
                return home_url( "estates/" . $type_slug . "/" . $post->post_name );
            } else {
                return $link;
            }
        }
    }
}

/** Redirect to url based on taxonomy Type and cpt Real Estate slug */
add_action( "init", "redirect_to_url" );
function redirect_to_url(){
    add_rewrite_rule(
        "estates/([a-zA-Z0-9-]+)/([a-zA-Z0-9-]+)?$",
        'index.php?post_type=real_estate&taxonomy=type&term=$matches[1]&name=$matches[2]'
    );
 }