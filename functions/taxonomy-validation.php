<?php
/**
 * Validation of Location and Type taxonomies
 */

/** cheking if Location and Type are choosen */
if(is_admin()) {
    function get_cpt_taxonomies( $real_estate_id ) {
        $taxonomies              = array(); 
        $taxonomies["location"]  = get_the_terms( $real_estate_id, "location" ); 
        $taxonomies["type"]      = get_the_terms( $real_estate_id, "type" );

        return $taxonomies;
    }

    /** returning post to draft status */
    function draft_update( $real_estate_id ) {
        $real_estate = array( "ID" => $real_estate_id, "post_status" => "draft" );
        wp_update_post( $real_estate );
    }

    /** validating if there is location and type assign to post and is there more then one */
    add_action( "publish_real_estate", "post_published_notification", 10, 2 );
    function post_published_notification() {
        if( isset( $_POST['post_ID'] ) ) {
            $taxonomies = get_cpt_taxonomies( $_POST['post_ID'] );
        }

        if( $taxonomies["location"] == false || $taxonomies["type"] == false ) {
            draft_update( $_POST["post_ID"] ); 
        } elseif( count( $taxonomies["location"] ) > 1 || count( $taxonomies["type"] ) > 1 ) {
            draft_update( $_POST["post_ID"] ); 
        }
    }

    /** Displaying notification if taxonomies are not chosen */
    add_action( "admin_notices", "general_admin_notice" );
    function general_admin_notice() {
        if( isset( $_GET["post"] ) ) {
            $taxonomies = get_cpt_taxonomies( $_GET["post"] );
        
            if( $taxonomies["location"] == false || $taxonomies["type"] == false ) {
                echo "<div class='notice notice-warning is-dismissible'>
                <p>The real estate is <strong>not published</strong>. Please choose one <strong>Location</strong> and one <strong>Type</strong>.</p>
                </div>";
            } elseif( count($taxonomies["location"]) > 1 || count($taxonomies["type"]) > 1 ) {
                echo "<div class='notice notice-warning is-dismissible'>
                <p>The real estate is <strong>not published</strong>. Please choose just <strong>one</strong> Location and <strong>one</strong> Type.</p>
                </div>";
            }
        }
    }
}