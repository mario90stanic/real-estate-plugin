<?php
/**
 *  Initialising single-post page for real estate cpt 
 */

add_filter( "single_template", "my_custom_template" );
function my_custom_template($single) {
    global $post;
    global $plugin_path;
    
    if ( $post->post_type == "real_estate" ) {
        if ( file_exists( $plugin_path . "views/single-real_estate.php" )) {
            return $plugin_path . "views/single-real_estate.php";
        }
    }

    return $single;
}