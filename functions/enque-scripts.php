<?php
/**
 * Enquing the scripts for the Real Estate plugin
 */

/** Enquing jQuery */
add_action( "wp_enqueue_scripts", "theme_scripts" );
function theme_scripts() {
    wp_enqueue_script( "jquery" );
}

/** Enquing custom javascript */
add_action( "wp_enqueue_scripts", "js_init" );
function js_init() {
    wp_enqueue_script( "real-estate", plugins_url( "real-estate-plugin/assets/js/real-estate.js" ) );
    wp_localize_script( "real-estate", "real_estate", array(
        "plugins_url" => plugins_url(),
    ));
}