<?php

/** 
 * Extending the wp serch scope fot the ACF subtite field and Location and Typetaxonomies
 */
add_filter( "posts_clauses_request", "extending_search_scope", 10, 2 );
function extending_search_scope( $pieces, $query ){
    
    global $wp_query;
  
    if ( ! is_admin() && isset($wp_query->query["s"] ) ) {
        $term            = $wp_query->query["s"];

        $term_location   = get_term_by( "name", $term, "location" );
        $term_type       = get_term_by( "name", $term, "type" );
        
        if ( is_object( $term_location ) ) {
            $tax         = $term_location->term_id;
        } elseif ( is_object($term_type ) ) {
            $tax         = $term_type->term_id;
        } else {
            $tax         = 0;
        }

        $pieces["join"]   = "INNER JOIN wp_postmeta ON (wp_posts.ID = wp_postmeta.post_id)
            INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id)";
        $pieces["where"]  = " AND (
            (wp_posts.post_title LIKE '%{$term}%')
            OR (wp_postmeta.meta_key = 'subtitle' AND wp_postmeta.meta_value LIKE '%{$term}%')
            OR (wp_term_relationships.term_taxonomy_id = {$tax})
            )";
        $pieces["groupby"] = "wp_posts.post_title";
    }

	return $pieces;
}