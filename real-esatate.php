<?php
/**
 * @package Real Estate Plugin
 * @version 1.0
 */

/*
Plugin Name: Real Estate Plugin
Description: Test for ForwardSlash.
Author: Mario Stanic
Version: 1.0
*/

$plugin_path = plugin_dir_path( __FILE__ );
require_once($plugin_path . "functions.php");
require_once($plugin_path . "core/class-base.php");
require_once($plugin_path . "core/class-register-cpt.php");
require_once($plugin_path . "core/class-registration-acf.php");

/** Registration CPT */
$cpt_name    = "Real estate";
$real_estate = new registration_cpt($cpt_name);
$real_estate->add_taxonomy("Location", "location");
$real_estate->add_taxonomy("Type", "estates");

/** Registration ACF */
new registration_acf($cpt_name);
